#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h> 
#include <conio.h>

#define MAX_NAME_LENGTH 51      // 50 characters + null terminator
#define DATE_LENGTH 11          // 10 characters for YYYY-MM-DD format + null terminator
#define REGISTRATION_LENGTH 8   // 7 characters for registration number + null terminator
#define MAX_PROGRAM_CODE_LENGTH 6 // 5 characters for program code + null terminator
#define MAX_ROWS 1024

typedef struct Student {
    char studentName[MAX_NAME_LENGTH];
    char studentdob[DATE_LENGTH];
    char studentRegNo[REGISTRATION_LENGTH];
    char program_code[MAX_PROGRAM_CODE_LENGTH];
    float annual_tuition;
} Student;
// function definitions
int compareStudentsByName(const void *a, const void *b) {
    const Student *studentA = (const Student *) a;
    const Student *studentB = (const Student *) b;

    return strcmp(studentA->studentName, studentB->studentName);
}

int compareStudentsByRegistrationNumber(const void *a, const void *b) {
    const Student *studentA = (const Student *) a;
    const Student *studentB = (const Student *) b;

    return strcmp(studentA->studentRegNo, studentB->studentRegNo);
}
// validating date
bool validateDate(const char *date) {
    if (strlen(date) != 10) {
        return false;
    }

    if (date[4] != '-' || date[7] != '-') {
        return false;
    }

    int year, month, day;
    if (sscanf(date, "%d-%d-%d", &year, &month, &day) != 3) {
        return false;
    }

    if (year < 0 || month < 1 || month > 12 || day < 1 || day > 31) {
        return false;
    }

    return true;
}
// validating the registration number
bool validateRegistrationNumber(const char *reg_number) {
    int len = strlen(reg_number);
    if (len != 6) { // 5digits and a null terminator
        return false;
    }

    for (int i = 0; i < len; i++) {
        if (!isdigit(reg_number[i])) {
            return false;
        }
    }

    return true;
}

void sortByName() {
    FILE *fileOne = fopen("studentInfo.bin", "rb");
    Student students[MAX_ROWS];
    int numStudents = 0;

    if (fileOne == NULL) {
        printf("\n\t\t\tError opening file!\n");
        return;
    }

    while (fread(&students[numStudents], sizeof(Student), 1, fileOne) == 1) {
        numStudents++;
    }

    qsort(students, numStudents, sizeof(Student), compareStudentsByName);

    printf("\n\t\t%-20s", "Name");
    printf("\n\t\t--------------------");

    for (int i = 0; i < numStudents; i++) {
        printf("\n\t\t%-20s", students[i].studentName);
    }

    printf("\n\n\t\tEnter any keys to continue.......");
    getch();

    fclose(fileOne);
}

void sortByRegistrationNumber() {
    FILE *fileOne = fopen("studentInfo.bin", "rb");
    Student students[MAX_ROWS];
    int numStudents = 0;

    if (fileOne == NULL) {
        printf("\n\t\t\tError opening file!\n");
        return;
    }

    while (fread(&students[numStudents], sizeof(Student), 1, fileOne) == 1) {
        numStudents++;
    }

    qsort(students, numStudents, sizeof(Student), compareStudentsByRegistrationNumber);

    printf("\n\t\t%-15s", "Registration Number");
    printf("\n\t\t--------------------");

    for (int i = 0; i < numStudents; i++) {
        printf("\n\t\t%-15s", students[i].studentRegNo);
    }

    printf("\n\n\t\tEnter any keys to continue.......");
    getch();

    fclose(fileOne);
}


void createAccount();
void displayInfo();
void updateInfo();
void deleteInfo();
void searchInfo();
void sortStudents();
void exportToCSV(); // Function to export data to CSV

int main() {
    char option;

    while (option!= '0') {
        system("cls");
        printf("\t\t====== Student Management Database System ======\n");
        printf("\n\t\t\t1. Create Student Account");
        printf("\n\t\t\t2. Display All Students Information");
        printf("\n\t\t\t3. Update Student Information");
        printf("\n\t\t\t4. Delete Student Information");
        printf("\n\t\t\t5. Search Student Information");
        printf("\n\t\t\t6. Sort Student Information");
        printf("\n\t\t\t7. Export Data to CSV");
        printf("\n\t\t\t0. Exit");

        printf("\n\n\n\t\t\tEnter Your Option: ");
        scanf(" %c", &option);

        switch (option) {
            case '1':
                createAccount();
                break;
            case '2':
                displayInfo();
                break;
            case '3':
                updateInfo();
                break;
            case '4':
                deleteInfo();
                break;
            case '5':
                searchInfo();
                break;
            case '6':
                sortStudents();
                break;
			case '7':
                exportToCSV(); // Call the export function
                break;
            case '0':
                printf("\n\t\t\t====== Thank You ======");
                break;
            default:
                printf("\n\t\t\tInvalid Option, Please Enter Right Option!\n");
        }
    }
    return 0;
}

void sortStudents() {
    char option;

    system("cls");
    printf("\t\t\t====== Sort Students ======\n");
    printf("\n\t\t\t1. Sort by Name");
    printf("\n\t\t\t2. Sort by Registration Number");
    printf("\n\t\t\t0. Back");

    printf("\n\n\n\t\t\tEnter Your Option: ");
    scanf(" %c", &option);

    switch (option) {
        case '1':
            sortByName();
            break;
        case '2':
            sortByRegistrationNumber();
            break;
        case '0':
            break;
        default:
            printf("\n\t\t\tInvalid Option, Please Enter Right Option!\n");
    }
}
void exportToCSV() {
    FILE *binFile = fopen("studentInfo.bin", "rb");
    FILE *csvFile = fopen("studentInfo1.csv", "a");

    if (binFile == NULL || csvFile == NULL) {
        printf("\n\t\t\tError opening file!\n");
        return;
    }

    Student student;
    fprintf(csvFile, "Name,RegNo,DOB,Program Code,Annual Tuition\n"); // Write CSV header

    while (fread(&student, sizeof(student), 1, binFile) == 1) {
        fprintf(csvFile, "%s,%s,%s,%s,%.2f\n", 
                student.studentName, student.studentRegNo, student.studentdob,
                student.program_code, student.annual_tuition);
    }

    fclose(binFile);
    fclose(csvFile);

    printf("\n\t\t\tData exported to studentInfo.csv successfully!\n");
    printf("\n\t\t\tEnter any keys to continue.......");
    getch();
}
void createAccount()
{
    FILE *fileOne = fopen("studentInfo.bin", "ab+");

    if (fileOne == NULL)
    {
        printf("\n\t\t\tError!\n");
    }

    Student studentInformation;

    system("cls");

    printf("\t\t\t====== Create Student Account ======\n");

    printf("\n\t\t\tEnter Student's Name : ");
    getchar();
    gets(studentInformation.studentName);

    while (1) {
        printf("\n\t\t\tEnter Student's Registration Number (6 digits) : ");
        gets(studentInformation.studentRegNo);
        if (validateRegistrationNumber(studentInformation.studentRegNo)) {
            break;
        }
        printf("\n\t\t\tInvalid Registration Number. Please enter a 6-digit number.\n");
    }

    while (1) {
        printf("\n\t\t\tEnter Student's Date of Birth (YYYY-MM-DD) : ");
        gets(studentInformation.studentdob);
        if (validateDate(studentInformation.studentdob)) {
            break;
        }
        printf("\n\t\t\tInvalid Date format. Please enter the date in YYYY-MM-DD format.\n");
    }
    printf("\t\t\tEnter Student's Program code: ");
    gets(studentInformation.program_code);
    strtok(studentInformation.program_code, "\n");

    while (true) {
        printf("\t\t\tEnter Student's annual tuition : ");
        scanf("%f", &studentInformation.annual_tuition);
        if (studentInformation.annual_tuition != 0) {
            break;
        }
        printf("\t\t\tAnnual tuition cannot be zero.\n");
    }

    fwrite(&studentInformation, sizeof(studentInformation), 1, fileOne);

    printf("\n\n\t\t\tInformations have been stored sucessfully\n");
    printf("\n\n\t\t\tEnter any keys to continue.......");
    getch();

    fclose(fileOne);
}
void displayInfo()
{
    FILE *fileOne = fopen("studentInfo.bin", "rb");

    if (fileOne == NULL)
    {
        printf("\n\t\t\tError!\n");
    }

    Student studentInformation;

    system("cls");

    printf("\t\t\t\t====== All Students Information ======\n");

    printf("\n\n\t\t%-20s%-15s%-10s%-20s%-12s\n", "Name", "Registration No.", "DOB", "Program Code", "Annual Tuition");
    printf("\t\t----------------------------------------------------------------------------------------");

    while (fread(&studentInformation, sizeof(studentInformation), 1, fileOne) == 1)
    {
        printf("\n\n\t\t%-20s%-15s%-10s%-20s%-12g", studentInformation.studentName, studentInformation.studentRegNo, studentInformation.studentdob, studentInformation.program_code, studentInformation.annual_tuition);
    }

    printf("\n\n\t\tEnter any keys to continue.......");
    getch();

    fclose(fileOne);
}
void updateInfo()
{
    FILE *fileOne = fopen("studentInfo.bin", "rb");
    FILE *temp = fopen("temp.bin", "wb");

    Student studentInformation, tempInformation;

    int choice, flag = 0;

    if (fileOne == NULL || temp == NULL)
    {
        printf("\n\t\t\tError!\n");
    }

    system("cls");

    printf("\t\t\t\t====== Update Students Information ======\n");

    printf("\n\t\t\tEnter Student's Registration Number : ");
    getchar();
    gets(tempInformation.studentRegNo);

    while (fread(&studentInformation, sizeof(studentInformation), 1, fileOne) == 1)
    {
        if (strcmp(studentInformation.studentRegNo, tempInformation.studentRegNo) == 0)
        {
            flag++;
            printf("\n\t\t\tChoose your option :\n\t\t\t1.Update Student Name\n\t\t\t2.Update Student DOB\n\t\t\t3.Update Student Program Code\n\t\t\t4.Update Student Annual Tuition");
            printf("\n\n\t\t\tEnter Your Option : ");
            scanf("%d", &choice);
            if (choice == 1)
            {
                printf("\n\t\t\tEnter Student's Name to Update: ");
                getchar();
                gets(tempInformation.studentName);
                strcpy(studentInformation.studentName, tempInformation.studentName);

                fwrite(&studentInformation, sizeof(studentInformation), 1, temp);
                printf("\n\n\t\t\tUpdated successfully!\n\n");
            }
            else if (choice == 2)
            {
                printf("\n\t\t\tEnter Student's DOB to Update: ");
                getchar();
                gets(tempInformation.studentdob);
                strcpy(studentInformation.studentdob, tempInformation.studentdob);

                fwrite(&studentInformation, sizeof(studentInformation), 1, temp);
                printf("\n\n\t\t\tUpdated successfully!\n\n");
            }
            else if (choice == 3)
            {
                printf("\n\t\t\tEnter Student's Program Code to Update: ");
                getchar();
                gets(tempInformation.program_code);
                strcpy(studentInformation.program_code, tempInformation.program_code);

                fwrite(&studentInformation, sizeof(studentInformation), 1, temp);
                printf("\n\n\t\t\tUpdated successfully!\n\n");
            }
            else if (choice == 4)
            {
                printf("\n\t\t\tEnter Student's Annual Tuition to Update: ");
                scanf("%f", &tempInformation.annual_tuition);
                studentInformation.annual_tuition = tempInformation.annual_tuition;

                fwrite(&studentInformation, sizeof(studentInformation), 1, temp);
                printf("\n\n\t\t\tUpdated successfully!\n\n");
            }
            else
            {
                printf("\n\t\t\tInvalid Option.");
                fwrite(&studentInformation, sizeof(studentInformation), 1, temp);
            }
}
        else
        {
            fwrite(&studentInformation, sizeof(studentInformation), 1, temp);
        }
    }

    fclose(fileOne);
    fclose(temp);

    remove("studentInfo.bin");
    rename("temp.bin", "studentInfo.bin");

    if (flag == 0)
    {
        printf("\n\t\t\tStudent Registration Number is not found");
    }

    printf("\n\n\t\t\tEnter any keys to continue.......");
    getch();
}

void deleteInfo()
{
    FILE *fileOne = fopen("studentInfo.bin", "rb");
    FILE *temp = fopen("temp.bin", "wb");

    Student studentInformation, tempInformation;

    int choice, flag = 0;

    if (fileOne == NULL || temp == NULL)
    {
        printf("\n\t\t\tError!\n");
    }

    system("cls");

    printf("\t\t\t\t====== Delete Student Information ======\n");

    printf("\n\t\t\tEnter Student's Registration Number : ");
    getchar();
    gets(tempInformation.studentRegNo);

    while (fread(&studentInformation, sizeof(studentInformation), 1, fileOne) == 1)
    {
        if (strcmp(studentInformation.studentRegNo, tempInformation.studentRegNo) == 0)
        {
            flag++;
            printf("\n\t\t\tAre you sure to delete ??\n\t\t\t\t1.Yes\n\t\t\t\t2.Back\n\t\t\t\tEnter Your Option : ");
            scanf("%d", &choice);
            if (choice == 1)
            {
                printf("\n\n\t\t\tInformation has been deleted successfully!\n\n");
            }
            else if (choice == 2)
            {
                fwrite(&studentInformation, sizeof(studentInformation), 1, temp);
            }
            else
            {
                printf("\n\t\t\tInvalid Option");
                fwrite(&studentInformation, sizeof(studentInformation), 1, temp);
            }
        }
        else
        {
            fwrite(&studentInformation, sizeof(studentInformation), 1, temp);
        }
    }

    fclose(fileOne);
    fclose(temp);

    remove("studentInfo.bin");
    rename("temp.bin", "studentInfo.bin");

    if (flag == 0)
    {
        printf("\n\t\t\tStudent Registration Number is not found");
    }

    printf("\n\n\t\t\tEnter any keys to continue.......");
    getch();
}

void searchInfo()
{
    FILE *fileOne = fopen("studentInfo.bin", "rb");

    Student studentInformation;

    int choice, flag = 0;
    char studentName[MAX_NAME_LENGTH], studentRegNo[REGISTRATION_LENGTH];

    if (fileOne == NULL)
    {
        printf("\n\t\t\tError!\n");
    }

    system("cls");

    printf("\t\t\t\t====== Search Student Information ======\n");

    printf("\n\t\t\tChoose your option :\n\t\t\t1.Search by Student Name\n\t\t\t2.Search by Student Registration Number");
    printf("\n\n\t\t\tEnter Your Option : ");
    scanf("%d", &choice);

    if (choice == 1)
    {
        system("cls");
        printf("\t\t\t\t====== Search Student Information ======\n");
        printf("\n\n\t\t\tEnter Student Name : ");
        getchar();
        gets(studentName);
        while (fread(&studentInformation, sizeof(studentInformation), 1, fileOne) == 1)
        {
            if (stricmp(studentInformation.studentName, studentName) == 0)
            {
                flag++;
                printf("\n\t\t\tStudent Name : %s\n\t\t\tStudent Registration Number : %s\n\t\t\tStudent DOB : %s\n\t\t\tStudent Program Code : %s\n\t\t\tStudent Annual Tuition : %.2f\n", studentInformation.studentName, studentInformation.studentRegNo, studentInformation.studentdob, studentInformation.program_code, studentInformation.annual_tuition);
            }
        }
        if (flag == 0)
        {
            printf("\n\t\t\tStudent Name is not found");
        }
    }
    else if (choice == 2)
    {
        system("cls");
        printf("\t\t\t\t====== Search Student Information ======\n");
        printf("\n\n\t\t\tEnter Student Registration Number : ");
        getchar();
        gets(studentRegNo);
        while (fread(&studentInformation, sizeof(studentInformation), 1, fileOne) == 1)
        {
            if (strcmp(studentInformation.studentRegNo, studentRegNo) == 0)
            {
                flag++;
                printf("\n\t\t\tStudent Name : %s\n\t\t\tStudent Registration Number : %s\n\t\t\tStudent DOB : %s\n\t\t\tStudent Program Code : %s\n\t\t\tStudent Annual Tuition : %.2f\n", studentInformation.studentName, studentInformation.studentRegNo, studentInformation.studentdob, studentInformation.program_code, studentInformation.annual_tuition);
            }
        }
        if (flag == 0)
        {
            printf("\n\t\t\tStudent Registration Number is not found");
        }
    }
    else
    {
        printf("\n\t\t\tInvalid Option");
    }

    fclose(fileOne);

    printf("\n\n\n\t\t\tEnter any keys to continue.......");
    getch();
}
