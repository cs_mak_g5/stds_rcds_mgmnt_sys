# stds_rcds_mgmnt_sys



## Students Records management system
This repository contains a simple student management database system implemented in C. The system allows users to perform various operations such as creating student accounts, displaying student information, updating student information, deleting student information, searching for students, sorting students, and exporting student data to a CSV file.


## Data Structure Selection Considerations
The student information in this system is stored as an array of Student structures. Below are some considerations for selecting this data structure:

1. Simplicity: Using an array of structures simplifies the implementation of the database system. Each element of the array represents a student, and each field within the structure represents a piece of information about the student (e.g., name, registration number, date of birth, program code, annual tuition)

2. Efficiency for Small Datasets: For small datasets, such as student records in this system, an array provides efficient access and manipulation. It offers constant-time access to elements based on their indices, making it suitable for operations like sorting and searching.

3. Sorting and Searching: Arrays facilitate sorting and searching operations, which are essential functionalities in a database system. The qsort function from the standard library is used for sorting students by name and registration number, leveraging the array structure's simplicity and efficiency.

4. Flexibility: Arrays allow for easy addition and removal of elements, which aligns well with the dynamic nature of student management systems. While the size of the array is fixed in this implementation (MAX_ROWS), it can be adjusted based on the expected number of students or dynamically allocated if needed.

5. Ease of File Handling: Storing student records in an array simplifies file handling operations. Data can be easily read from and written to a binary file using functions like fwrite and fread. This approach offers straightforward serialization and deserialization of data.

## Owners

```
1. katende Herman
2. Muwanguzi Samuel
3. Odongo Emmanuel
4. Janet Mayanja
5. Rania Nabatanzi
```

## links to the vidoes Explaning how the code works

1.Muwangu Samuel: https://youtu.be/snxV5X2hdgs?si=IOi37cOjC7rmQ_nr

2.Nabatanzi Rania: https://youtu.be/zmPq5N-DAXk

3.Mayanja Janet: https://youtu.be/UcvyvZRa5QY

4.Emmanuel Odongo: https://youtu.be/9Gf1fn-lOXM

5.herman katende: https://youtu.be/LeKQWhRjlFI?si=MvACVCbJbbb95Yxe


## File access

```
cd existing_repo
git remote :  https://gitlab.com/cs_mak_g5/stds_rcds_mgmnt_sys.git

```

